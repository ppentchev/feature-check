#!/usr/bin/perl
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause


use v5.10;
use strict;
use warnings;

package Test::FeatureCheck;

use v5.10;
use strict;
use warnings;

use version; our $VERSION = version->declare("v2.2.0");

use parent qw(Exporter);

our @EXPORT_OK = qw(
	env_init
	get_error_output get_ok_output
	test_fcheck_init
);

sub env_init()
{
	my $prog = $ENV{TEST_PROG} // './feature-check';

	return (
		fcheck => $ENV{TEST_FCHECK} // './t/bin/fcheck',
		is_python => $prog =~ m{ /python/ }x ? 1 : 0,
		is_rust => $prog =~ m{ /rust/ }x ? 1 : 0,
		prog => $prog,
	);
}

sub get_ok_output($ $) {
	my ($cmd, $desc) = @_;

	my $c = Test::Command->new(cmd => $cmd);
	$c->exit_is_num(0, "$desc succeeded");
	$c->stderr_is_eq('', "$desc did not output any errors");
	split /\n/, $c->stdout_value
}

sub get_error_output($ $) {
	my ($cmd, $desc) = @_;

	my $c = Test::Command->new(cmd => $cmd);
	$c->exit_isnt_num(0, "$desc failed");
	$c->stdout_is_eq('', "$desc did not output anything");
	split /\n/, $c->stderr_value
}

sub test_fcheck_init($) {
	my ($c) = @_;
	my $fcheck = $c->{fcheck};

	if (! -f $fcheck || ! -x $fcheck) {
		Test::More::BAIL_OUT("The '$fcheck' program does not exist ".
		    "or is not executable");
	}
	$ENV{FCHECK_TEST_OPT} = '--features';
	$ENV{FCHECK_TEST_PREFIX} = 'Features: ';
	my $fcheck_base;
	Test::More::subtest 'Make sure the fcheck helper works' => sub {
		my @lines = get_ok_output([$fcheck, '--features'],
		    'fcheck features');
		Test::More::is scalar @lines, 1,
		    'fcheck --features output a single line';
		Test::More::BAIL_OUT("No 'Features: ' on the '$fcheck' ".
		    "features line") unless
		    $lines[0] =~ /^ Features: \s (?<features> .* ) $/x;
		my @words = split /\s+/, $+{features};
		my %names = map {
			my @res = split /[:\/=]/, $_, 2;
			scalar(@res) == 2 ? @res : (@res, "1.0")
		} @words;
		Test::More::BAIL_OUT("No 'base' in the '$fcheck' ".
		    "features list") unless defined $names{base};
		$fcheck_base = $names{base};
		Test::More::BAIL_OUT("Found 'x' in the '$fcheck' ".
		    "features list") if defined $names{x};
	};
	return $fcheck_base;
}

1;

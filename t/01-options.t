#!/usr/bin/perl
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

use v5.010;
use strict;
use warnings;

use Test::More;
use Test::Command;

use lib 't/lib';
use Test::FeatureCheck qw(env_init get_error_output get_ok_output);

my %c = env_init;

my $version_line;
my @usage_lines;

plan tests => 8;

# A single version line with -V
subtest 'Version output with -V' => sub {
	my $c = Test::Command->new(cmd => [$c{prog}, '-V']);
	$c->exit_is_num(0, '-V succeeded');
	$c->stderr_is_eq('', '-V did not output any errors');
	my @lines = split /\n/, $c->stdout_value, -1;
	BAIL_OUT('Unexpected number of lines in the -V output') unless @lines == 2;
	BAIL_OUT('Unexpected -V line') unless $lines[0] =~ /^ feature-check \s \S+ $/x;
	$version_line = $lines[0];
};

# More than one usage line with -h
subtest 'Usage output with -h' => sub {
	my $c = Test::Command->new(cmd => [$c{prog}, '-h']);
	$c->exit_is_num(0, '-h succeeded');
	$c->stderr_is_eq('', '-h did not output any errors');
	my @lines = split /\n/, $c->stdout_value;
	BAIL_OUT('Too few lines in the -h output') unless @lines > 1;
	BAIL_OUT('Unexpected -h first line') unless $lines[0] =~ /^ Usage: .* \s feature[_-]check /x;
	@usage_lines = @lines;
};

if (!$c{is_python}) {
	subtest 'Usage and version output with -V -h' => sub {
		my @lines = get_ok_output([$c{prog}, '-V', '-h'], '-V -h');
		is scalar @lines, 1 + @usage_lines, '-V -h output one more line than the usage message';
		is $lines[0], $version_line, '-V -h output the version line first';
		shift @lines;
		is_deeply \@lines, \@usage_lines, '-V -h output the usage message';
	};

	subtest 'Usage and version output with -hV' => sub {
		my @lines = get_ok_output([$c{prog}, '-hV'], '-hV');
		is scalar @lines, 1 + @usage_lines, '-hV output one more line than the usage message';
		is $lines[0], $version_line, '-hV output the version line first';
		shift @lines;
		is_deeply \@lines, \@usage_lines, '-hV output the usage message';
	};
} else {
	subtest 'Version output with -V -h' => sub {
		my @lines = get_ok_output([$c{prog}, '-V', '-h'], '-V -h');
		is_deeply \@lines, [$version_line], '-V -h output the version message';
	};

	subtest 'Usage output with -hV' => sub {
		my @lines = get_ok_output([$c{prog}, '-hV'], '-hV');
		is_deeply \@lines, \@usage_lines, '-hV output the usage message';
	};
}

subtest 'Long-form version' => sub {
	my @lines = get_ok_output([$c{prog}, '--version'], '--version');
	is scalar @lines, 1, '--version output a single line';
	is $lines[0], $version_line, '--version output the version information';
};

subtest 'Long-form usage' => sub {
	my @lines = get_ok_output([$c{prog}, '--help'], '--help');
	ok @lines > 1, '--help output more than one line';
	is_deeply \@lines, \@usage_lines, '--help output the usage information';
};

subtest 'Invalid short option' => sub {
	my @lines = get_error_output([$c{prog}, '-X', 'Makefile'],
	    'invalid short option -X');
	SKIP: {
		skip 'Usage with unknown options is weird with Python and Rust', 2 if $c{is_python} || $c{is_rust};
		ok scalar @lines > scalar @usage_lines, '-X output more lines than the usage message';
		isnt index(join("\n", @lines), join("\n", @usage_lines)), -1, 
		    '-X output the usage message';
	}
};

subtest 'Invalid long option' => sub {
	my @lines = get_error_output([$c{prog}, '--whee', 'Makefile'],
	    'invalid short option --whee');
	SKIP: {
		skip 'Usage with unknown options is weird with Python and Rust', 2 if $c{is_python} || $c{is_rust};
		ok scalar @lines > scalar @usage_lines, '--whee output more lines than the usage message';
		isnt index(join("\n", @lines), join("\n", @usage_lines)), -1, 
		    '-whee output the usage message';
	}
};

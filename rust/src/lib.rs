#![warn(missing_docs)]
/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */
//! Query a program for supported features.
//!
//! The [`obtain::obtain_features`]
//! function queries a program for the features that it supports
//! via various methods (e.g. running it with the `--features`
//! command-line option) and allows other programs to check for
//! the presence and, possibly, versions of specific features.
//!
//! ```rust
//! # use anyhow::{bail, Result};
//!
//! use feature_check::defs::{Config, Obtained};
//! use feature_check::obtain;
//!
//! # fn main() -> Result<()> {
//! match obtain::obtain_features(&Config::default()
//!     .with_program("confget".to_string()))? {
//!     Obtained::NotSupported => eprintln!("Feature query not supported"),
//!     Obtained::Failed(err) => eprintln!("Could not query for features: {err}"),
//!     Obtained::Features(res) => {
//!         let mut features: Vec<&String> = res.keys().collect();
//!         features.sort_unstable();
//!         for name in &features {
//!             println!("{name}");
//!         }
//!     },
//!     other => bail!("Unexpected obtain_features() result: {:?}", other)
//! }
//! # Ok(())
//! # }
//! ```

#![doc(html_root_url = "https://docs.rs/feature-check/2.2.0")]

pub mod defs;
pub mod expr;
pub mod obtain;
pub mod version;

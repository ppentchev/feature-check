/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */
//! A set of parsers for the query expressions supported by feature-check.

use std::collections::HashMap;

use crate::defs::{Mode, ParseError};
use crate::version::{ParseError as VParseError, Version};

pub mod p_nom;

/// Parse a "feature" or "feature op version" expression for later evaluation.
///
/// Returns either [`Mode::Single`] or [`Mode::Simple`].
///
/// # Errors
///
/// Will return an error if the expression is neither a single feature name nor in
/// the "var op value" format or if an unrecognized comparison operator is specified.
#[inline]
pub fn parse_expr(expr: &str) -> Result<Mode, ParseError> {
    p_nom::parse_expr(expr)
}

/// Parse a version string.
///
/// # Errors
///
/// Returns an error if the version string is invalid.
#[inline]
pub fn parse_version(value: &str) -> Result<Version, VParseError> {
    p_nom::parse_version(value)
}

/// Parse a line of `feature[=version]` pairs.
///
/// # Errors
///
/// Returns an error if the feature names or version strings are invalid.
#[inline]
pub fn parse_features_line(value: &str) -> Result<HashMap<String, Version>, ParseError> {
    p_nom::parse_features_line(value)
}

<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

Things to do for feature-check
==============================

- output the version of the feature if requested
- checks and possibly complex expressions
- parse different input (feature list) formats

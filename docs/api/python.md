<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# The feature-check Python API

## Commonly-used functions

::: feature_check.obtain_features

::: feature_check.parse_expr

::: feature_check.parser.parse_features_line

::: feature_check.parse_version

::: feature_check.version.version_compare

## Commonly-used functions

::: feature_check.obtain.obtain_features

## Defaults

::: feature_check.defs.DEFAULT_OPTION

::: feature_check.defs.DEFAULT_PREFIX

::: feature_check.defs.DEFAULT_OUTPUT_FMT

## Version strings

::: feature_check.version.Version

::: feature_check.version.VersionComponent

## Expressions

::: feature_check.defs.Expr

::: feature_check.expr.ExprFeature

::: feature_check.expr.ExprVersion

::: feature_check.expr.BoolOp

::: feature_check.expr.ExprOp

::: feature_check.defs.Result

::: feature_check.expr.ResultBool

::: feature_check.expr.ResultVersion

## Errors

::: feature_check.defs.FCError

::: feature_check.obtain.ObtainError

::: feature_check.obtain.ObtainExecError

::: feature_check.obtain.ObtainNoFeaturesError

::: feature_check.obtain.ObtainNoFeaturesSupportError

::: feature_check.parser.ParseError

## Main program operating modes

::: feature_check.defs.Mode

::: feature_check.defs.ModeList

::: feature_check.defs.ModeSimple

::: feature_check.defs.ModeSingle

## Miscellaneous

::: feature_check.defs.VERSION

<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Download

These are the released versions of [feature-check](index.md) available for download.

## [2.2.0] - 2024-07-29

### Source tarball

- [feature-check-2.2.0.tar.gz](https://devel.ringlet.net/files/misc/feature-check/feature-check-2.2.0.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-2.2.0.tar.gz.asc))
- [feature-check-2.2.0.tar.bz2](https://devel.ringlet.net/files/misc/feature-check/feature-check-2.2.0.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-2.2.0.tar.bz2.asc))
- [feature-check-2.2.0.tar.xz](https://devel.ringlet.net/files/misc/feature-check/feature-check-2.2.0.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-2.2.0.tar.xz.asc))


## [2.1.0] - 2024-02-10

### Source tarball

- [feature-check-2.1.0.tar.gz](https://devel.ringlet.net/files/misc/feature-check/feature-check-2.1.0.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-2.1.0.tar.gz.asc))
- [feature-check-2.1.0.tar.bz2](https://devel.ringlet.net/files/misc/feature-check/feature-check-2.1.0.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-2.1.0.tar.bz2.asc))
- [feature-check-2.1.0.tar.xz](https://devel.ringlet.net/files/misc/feature-check/feature-check-2.1.0.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-2.1.0.tar.xz.asc))

## [2.0.0] - 2022-10-12

### Source tarball

- [feature-check-2.0.0.tar.gz](https://devel.ringlet.net/files/misc/feature-check/feature-check-2.0.0.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-2.0.0.tar.gz.asc))
- [feature-check-2.0.0.tar.bz2](https://devel.ringlet.net/files/misc/feature-check/feature-check-2.0.0.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-2.0.0.tar.bz2.asc))
- [feature-check-2.0.0.tar.xz](https://devel.ringlet.net/files/misc/feature-check/feature-check-2.0.0.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-2.0.0.tar.xz.asc))

## [1.0.1] - 2021-12-17

### Source tarball

- [feature-check-1.0.1.tar.gz](https://devel.ringlet.net/files/misc/feature-check/feature-check-1.0.1.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-1.0.1.tar.gz.asc))
- [feature-check-1.0.1.tar.bz2](https://devel.ringlet.net/files/misc/feature-check/feature-check-1.0.1.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-1.0.1.tar.bz2.asc))
- [feature-check-1.0.1.tar.xz](https://devel.ringlet.net/files/misc/feature-check/feature-check-1.0.1.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-1.0.1.tar.xz.asc))

## [1.0.0] - 2021-06-24

### Source tarball

- [feature-check-1.0.0.tar.gz](https://devel.ringlet.net/files/misc/feature-check/feature-check-1.0.0.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-1.0.0.tar.gz.asc))
- [feature-check-1.0.0.tar.bz2](https://devel.ringlet.net/files/misc/feature-check/feature-check-1.0.0.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-1.0.0.tar.bz2.asc))
- [feature-check-1.0.0.tar.xz](https://devel.ringlet.net/files/misc/feature-check/feature-check-1.0.0.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-1.0.0.tar.xz.asc))

## [0.2.2] - 2019-01-09

### Source tarball

- [feature-check-0.2.2.tar.gz](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.2.2.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.2.2.tar.gz.asc))
- [feature-check-0.2.2.tar.bz2](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.2.2.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.2.2.tar.bz2.asc))
- [feature-check-0.2.2.tar.xz](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.2.2.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.2.2.tar.xz.asc))

## [0.2.1] - 2018-11-23

### Source tarball

- [feature-check-0.2.1.tar.gz](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.2.1.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.2.1.tar.gz.asc))
- [feature-check-0.2.1.tar.bz2](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.2.1.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.2.1.tar.bz2.asc))
- [feature-check-0.2.1.tar.xz](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.2.1.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.2.1.tar.xz.asc))

## [0.2.0] - 2018-11-22

### Source tarball

- [feature-check-0.2.0.tar.gz](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.2.0.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.2.0.tar.gz.asc))
- [feature-check-0.2.0.tar.bz2](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.2.0.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.2.0.tar.bz2.asc))
- [feature-check-0.2.0.tar.xz](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.2.0.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.2.0.tar.xz.asc))

## [0.1.1] - 2018-05-08

### Source tarball

- [feature-check-0.1.1.tar.gz](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.1.1.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.1.1.tar.gz.asc))
- [feature-check-0.1.1.tar.bz2](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.1.1.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.1.1.tar.bz2.asc))
- [feature-check-0.1.1.tar.xz](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.1.1.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.1.1.tar.xz.asc))

## [0.1.0] - 2018-04-22

### Source tarball

- [feature-check-0.1.0.tar.gz](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.1.0.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.1.0.tar.gz.asc))
- [feature-check-0.1.0.tar.bz2](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.1.0.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.1.0.tar.bz2.asc))
- [feature-check-0.1.0.tar.xz](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.1.0.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/feature-check/feature-check-0.1.0.tar.xz.asc))

[2.2.0]: https://gitlab.com/ppentchev/feature-check/-/tags/release%2F2.2.0
[2.1.0]: https://gitlab.com/ppentchev/feature-check/-/tags/release%2F2.1.0
[2.0.0]: https://gitlab.com/ppentchev/feature-check/-/tags/release%2F2.0.0
[1.0.1]: https://gitlab.com/ppentchev/feature-check/-/tags/release%2F1.0.1
[1.0.0]: https://gitlab.com/ppentchev/feature-check/-/tags/release%2F1.0.0
[0.2.2]: https://gitlab.com/ppentchev/feature-check/-/tags/release%2F0.2.2
[0.2.1]: https://gitlab.com/ppentchev/feature-check/-/tags/release%2F0.2.1
[0.2.0]: https://gitlab.com/ppentchev/feature-check/-/tags/release%2F0.2.0
[0.1.1]: https://gitlab.com/ppentchev/feature-check/-/tags/release%2F0.1.1
[0.1.0]: https://gitlab.com/ppentchev/feature-check/-/tags/release%2F0.1.0

# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

{ pkgs ? import <nixpkgs> { }
, py-ver ? "12"
}:
let
  python-name = "python3${py-ver}";
  python = builtins.getAttr python-name pkgs;
  python-pkgs = python.withPackages (p: with p;
    [
      mkdocs
      mkdocs-material
      mkdocstrings
      mkdocstrings-python
    ]
  );
in
pkgs.mkShell {
  buildInputs = [ python-pkgs ];
  shellHook = ''
    set -e
    rm -rf site
    python3.${py-ver} -m mkdocs build
    exit
  '';
}

# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

{ pkgs ? import <nixpkgs> {}, perl-ver ? "38", }:
let
  perl-tree-name = "perl5${perl-ver}Packages";
  perl-tree = builtins.getAttr perl-tree-name pkgs;
  perl-pkgs = with perl-tree; [ perl JSONXS TestCommand ];
in pkgs.mkShell {
  buildInputs = [ perl-pkgs ];
  shellHook = ''
    set -e
    make test-single TEST_PROG=./perl5/feature-check.pl
    exit
  '';
}
